#!/usr/bin/perl -w

use strict;

my $file = $ARGV[0];
if (!$file) {
	die "a file must be given as a command line argument";
}

if (-f $file) {
	open(FIL, "<$file");
}
else {
	die "no such file, $file";
}

my $line = "";
my $section = "";
my $blank = 0;
foreach (<FIL>) {
	if (/^Looking into/) {
		$line = $_;
		if ($section and not $blank) {
			process($section);
			$blank = 1;
		}
		$section = $line;
	}
	else {
		$section .= $_;
		$blank = 0;
	}
}

sub process {
	my ($section) = @_;

	#if ($section =~ /^Looking into(.*?)\$(\n  .*?$)*\Z(?!\n)/sg) {
	#	 print "one gone\n";
	#	return;
	#}

	$section =~ s,\n  http://(.*)\(200\) OK$,,mgo;
	$section =~ s,\n  http://(.*)\(200\) Document,,mgo;
	$section =~ s,\n  http://(.*)\(200\) Request,,mgo;
	$section =~ s,\n  (http://\S+).*?\(301\) Moved Permanently. New URL: \1/\s$,,mg;
	$section =~ s,\n  http://(.*)\(302\) Found,,mgo;
	$section =~ s,\n  http://(.*)\(302\) Moved,,mgo;
	$section =~ s,\n  http://(.*)\(302\) Temporary,,mgo;
	$section =~ s,\n  http://(.*)\(302\) Object,,mgo;
	
	$section =~ s,\n  https://(.*)\(200\) OK$,,mgo;
	$section =~ s,\n  https://(.*)\(200\) Document,,mgo;
	$section =~ s,\n  https://(.*)\(200\) Request,,mgo;
	$section =~ s,\n  (https://\S+).*?\(301\) Moved Permanently. New URL: \1/\s$,,mg;
	$section =~ s,\n  https://(.*)\(302\) Found,,mgo;
	$section =~ s,\n  https://(.*)\(302\) Moved,,mgo;
	$section =~ s,\n  https://(.*)\(302\) Temporary,,mgo;
	$section =~ s,\n  https://(.*)\(302\) Object,,mgo;

	$section =~ s,\n  mailto:(.*)validity$,,mgo;
	$section =~ s,\n  mailto:(.*) invalid url$,,mgo;
	$section =~ s,\n  ftp:(.*)is ok$,,mgo;
	$section =~ s,\n  irc:(.*)validity$,,mgo;
	$section =~ s,\n  news:(.*)validity$,,mgo;
	$section =~ s,\n  news:(.*) invalid url$,,mgo;
	
	if ($section =~ m,^Looking into https://[\w/.-]+\n$,m) {
		return;
	}
	elsif ($section =~ m,^Looking into https://(.*?)\n  searching not continued in (.*) directory.\n$,m) {
		return;
	}
	$section =~ s/error\n/error /g;
	print $section;
	
}
